#!/usr/bin/python
# coding=UTF-8

from __future__ import print_function
import os, sys, re
from collections import namedtuple
import gitstatus
from subprocess import check_output, CalledProcessError, STDOUT

# Configuration at end

isRoot = os.getuid() == 0

Section = namedtuple('Section', ['fg', 'bg', 'generator'])

def __checkSudo():
    if isRoot:
        # Dont display the icon when we dont need to
        return False
    
    try:
        check_output(['sudo', '-nl'], stderr=STDOUT)
        return True
    except CalledProcessError:
        return False    

def __generateShortPath():
    currentPath = os.environ['PWD']
    currentDir = os.path.basename(currentPath)
    
    homeDir = os.environ['HOME']
    
    currentPath = re.sub(re.escape(homeDir), '~', currentPath, count=1)
    targetLength = max(PATH_MAX_LEN, len(currentDir))

    cutOffset = len(currentPath) - targetLength
    if cutOffset > 0:
        currentPath = u'{}{}'.format(PATH_TRUNC_CH, currentPath[cutOffset:])

    return currentPath

def sectionHostInfo():
    hasSudo = __checkSudo()
    if hasSudo:
        return u'{} \\u@\\h'.format(ICON_SUDO)
    return r'\u@\h'

def sectionPathInfo():
    if WRITABLE:
        return CURRENT_PATH
    else:
        return u'{} {}'.format(ICON_LOCK, CURRENT_PATH)

def sectionGit():
    status = gitstatus.get_git_status()
    if not status:
        return None

    content = status.branch

    if not status.isClean:
        content += '*'

    if status.remote:
        if status.remote == '.':
            upstream = ICON_OK
        else:
            upstream = status.remote \
                .replace('__AHEAD__', ICON_AHEAD) \
                .replace('__BEHIND__', ICON_BEHIND)
    else:
        upstream = '?'
        
    content += ' ' + upstream
    
    return content

def sectionBGJobs():
    if JOBS_RUNNING == 0 and JOBS_STOPPED == 0:
        return None

    return u'{} {}'.format(ICON_BACKGROUND_JOBS, JOBS_RUNNING + JOBS_STOPPED)

def __colourCode(fg, bg, bold=False):
    code = r'\[\033['

    if fg is None and bg is None:
        code += '0'
    else:
        if fg is None:
            code += '39'
        else:
            code += '38;5;' + str(fg)

        code += ';'
        
        if bg is None:
            code += '49'
        else:
            code += '48;5;' + str(bg)

    if bold:
        code += ';1'

    return code + 'm\]'

def titlePart():
    term = os.environ['TERM']
    if term.startswith('xterm') or term.startswith('rxvt'):
        return u'\\[\\033]0;\\u:{}\\007\]'.format(CURRENT_PATH)
    else:
        return ''

def generatePrompt():
    prompt = ''
    lastSection = None

    for section in SECTIONS:
        content = section.generator()

        if not content:
            continue

        if lastSection:
            # Transition to new section
            prompt += __colourCode(lastSection.bg, section.bg)
            prompt += SECTION_SEPARATOR
        
        prompt += __colourCode(section.fg, section.bg, True)
        prompt += u' {} '.format(content)

        lastSection = section

    # Final terminator
    prompt += __colourCode(lastSection.bg, None)
    prompt += SECTION_SEPARATOR
    prompt += __colourCode(None, None)
    prompt += __colourCode(231, None)

    if isRoot:
        prompt += r'\n# '
    else:
        prompt += r'\n$ '

    prompt += __colourCode(None, None)
    return titlePart() + prompt

# ================
# CONFIGURATION:
# ================

PATH_MAX_LEN = 25
PATH_TRUNC_CH = u'\u2026'
ICON_SUDO = u'\u29E8'

if 'TERM_PROGRAM' in os.environ and os.environ['TERM_PROGRAM'] == 'vscode':
    # VS Code specifics
    SECTION_SEPARATOR = u'\uEFCF'

    ICON_GIT = u'\uE0A0'
    ICON_BACKGROUND_JOBS = u'\u26ED'
    ICON_AHEAD = u'\u2191'
    ICON_BEHIND = u'\u2193'
    ICON_OK = u'\u2714'
    ICON_LOCK = u'\u26BF'
else:
    SECTION_SEPARATOR = u'\uE0B0' # Powerline Triangle

    ICON_GIT = u'\uE0A0'
    ICON_BACKGROUND_JOBS = u'\u26ED'
    ICON_AHEAD = u'\u2191'
    ICON_BEHIND = u'\u2193'
    ICON_OK = u'\u2714'
    ICON_LOCK = u'\uE0A2'

if isRoot:
    SECTIONS = [
        Section(255, 196, sectionHostInfo),
        Section(231, 237, sectionPathInfo),
        Section(255, 70, sectionBGJobs),
        Section(238, 148, sectionGit)
    ]
else:
    SECTIONS = [
        Section(255, 25, sectionHostInfo),
        Section(255, 31, sectionPathInfo),
        Section(255, 70, sectionBGJobs),
        Section(238, 148, sectionGit)
    ]

# ================
# END CONFIGURATION
# ================

if __name__ == '__main__':
    JOBS_STOPPED = int(sys.argv[1])
    JOBS_RUNNING = int(sys.argv[2])
    WRITABLE = int(sys.argv[3]) == 1

    CURRENT_PATH = __generateShortPath()

    prompt = generatePrompt()
    print(prompt.encode('utf-8'))


    