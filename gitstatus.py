#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Copyright (c) 2016, Martin Gondermann
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

This module defines a Print function to use with python 2.x or 3.x., so we can use the prompt with older versions of
Python too

It's interface is that of python 3.0's print. See
http://docs.python.org/3.0/library/functions.html?highlight=print#print

Shamelessly ripped from
http://www.daniweb.com/software-development/python/code/217214/a-print-function-for-different-versions-of-python
"""
# change those symbols to whatever you prefer
symbols = {'ahead of': '__AHEAD__', 'behind': '__BEHIND__', 'prehash': ':'}

import sys
import re
from subprocess import Popen, PIPE
from collections import namedtuple

Status = namedtuple('Status', ['branch', 'remote', 'staged', 'conflicts', 'changed', 'untracked', 'stashed', 'isClean'])

def get_tag_or_hash():
    cmd = Popen(['git', 'describe', '--exact-match'], stdout=PIPE, stderr=PIPE)
    so, se = cmd.communicate()
    tag = '%s' % so.decode('utf-8').strip()

    if tag:
        return tag
    else:
        cmd = Popen(['git', 'rev-parse', '--short', 'HEAD'], stdout=PIPE, stderr=PIPE)
        so, se = cmd.communicate()
        hash_name = '%s' % so.decode('utf-8').strip()
        return ''.join([symbols['prehash'], hash_name])


def get_stash():
    cmd = Popen(['git', 'rev-parse', '--git-dir'], stdout=PIPE, stderr=PIPE)
    so, se = cmd.communicate()
    stash_file = '%s%s' % (so.decode('utf-8').rstrip(), '/logs/refs/stash')

    try:
        with open(stash_file) as f:
            return sum(1 for _ in f)
    except IOError:
        return 0

def get_git_status():
    # `git status --porcelain --branch` can collect all information
    # branch, remote_branch, untracked, staged, changed, conflicts, ahead, behind
    po = Popen(['git', 'status', '--porcelain', '--branch'], env={'LC_ALL': 'C'}, stdout=PIPE, stderr=PIPE)
    stdout, stderr = po.communicate()
    if po.returncode != 0:
        return None  # Not a git repository

    # collect git status information
    untracked, staged, changed, conflicts = [], [], [], []
    num_ahead, num_behind = 0, 0
    ahead, behind = '', ''
    branch = None
    remote = None
    status = [(line[0], line[1], line[2:]) for line in stdout.decode('utf-8').splitlines()]
    for st in status:
        if st[0] == '#' and st[1] == '#':
            if re.search('Initial commit on', st[2]):
                branch = st[2].split(' ')[-1]
            elif re.search('No commits yet on', st[2]):
                branch = st[2].split(' ')[-1]
            elif re.search('no branch', st[2]):  # detached status
                branch = get_tag_or_hash()
            elif len(st[2].strip().split('...')) == 1:
                branch = st[2].strip()
            else:
                # current and remote branch info
                branch, rest = st[2].strip().split('...')
                if len(rest.split(' ')) == 1:
                    # remote_branch = rest.split(' ')[0]
                    remote = None
                else:
                    # ahead or behind
                    divergence = ' '.join(rest.split(' ')[1:])
                    divergence = divergence.lstrip('[').rstrip(']')
                    for div in divergence.split(', '):
                        if 'ahead' in div:
                            num_ahead = int(div[len('ahead '):].strip())
                            ahead = '%s%s' % (symbols['ahead of'], num_ahead)
                        elif 'behind' in div:
                            num_behind = int(div[len('behind '):].strip())
                            behind = '%s%s' % (symbols['behind'], num_behind)
                    remote = ''.join([behind, ahead])
        elif st[0] == '?' and st[1] == '?':
            untracked.append(st)
        else:
            if st[1] == 'M':
                changed.append(st)
            if st[0] == 'U':
                conflicts.append(st)
            elif st[0] != ' ':
                staged.append(st)

    stashedCount = get_stash()
    if not changed and not staged and not conflicts and not untracked and not stashedCount:
        isClean = True
    else:
        isClean = False

    if remote == "":
        remote = '.'

    if remote:
        remote = remote.decode('utf-8')

    return Status(
        branch,
        remote,
        len(staged),
        len(conflicts),
        len(changed),
        len(untracked),
        stashedCount,
        isClean
    )
