## Prerequisites
The following needs to be installed:
- **Powerline Fonts**
- **Python 2.14+**

## Installation

Copy this directory wherever you wish to install it.
This location will be known as `<path_to_dir>`.

Add the following to your `~/.bashrc`

    UPROMPT_DIR=<path_to_dir>
    PROMPT_COMMAND='source $UPROMPT_DIR/gen_prompt.sh'

The environment var `UPROMPT_DIR` is needed for other parts of the script to function.

## Features
* Current hostname
* Current user
* Path (limited in length)
* Git repo status
* Number of jobs (stopped and running)
* Writable path indicator
* Sudo cached credentials indicator
* Root user highlighting
