#!/bin/sh

if [ -z "$UPROMPT_DIR" ]; then
    echo "The environment var UPROMPT_DIR must be set to the location of the prompt"
    exit 1
fi

generate_prompt() {
    local JOBS_STOP=`jobs -s | wc -l`
    local JOBS_RUN=`jobs -r | wc -l`
    local WRITABLE=0
    if [ -w "$PWD" ]; then
        local WRITABLE=1
    fi

    export PS1=`/usr/bin/python $UPROMPT_DIR/prompt.py $JOBS_STOP $JOBS_RUN $WRITABLE`
}

generate_prompt
